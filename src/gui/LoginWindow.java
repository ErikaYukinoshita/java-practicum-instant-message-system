package gui;

import client.Main;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.AttributeSet;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

public class LoginWindow extends JFrame {
    JPanel contentPanel;
    public LoginWindow() {
        setTitle("学生数据库系统");
        setLocation(800,100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Exit the application and close the window
        setSize(420, 270);
        this.contentPanel = new LoginPanel();
        this.contentPanel.requestFocusInWindow();
        setContentPane(contentPanel);
        ImageIcon backgroundIcon = new ImageIcon("images/deep_sea_by_w00den_sp00n.jpg");
        JLabel backgroundLabel=new JLabel(backgroundIcon);
        backgroundLabel.setBounds(0,0,backgroundIcon.getIconWidth(),backgroundIcon.getIconHeight());
        getLayeredPane().add(backgroundLabel,new Integer(Integer.MIN_VALUE));
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        setVisible(true);
    }

    public class LoginPanel extends JPanel{
        JTextField nameField = new JTextField("用户名",12);
        //Document numDoc = numField.getDocument();
        JPasswordField pwField = new JPasswordField("密码");
        JPanel loginHeadPanel = new JPanel();
        JLabel headline = new JLabel("学生数据库系统");
        JPanel loginTextPanel = new JPanel();
        JLabel username = new JLabel("");
        JLabel password = new JLabel("");
        ImageIcon loginIcon = new ImageIcon("images/login_button.png");
        JButton logInButton = new LoginButton(loginIcon);
        Font msYaHei = new Font("Microsoft YaHei",0,18);
        boolean change=false;
        JLabel messageOnFail = new JLabel(" ");
        JLabel emptyLabel = new JLabel("");

        public class LoginButton extends JButton{
            public LoginButton(Icon icon) {
                super(icon);
                setBorderPainted(false);
                setBorder(null);
                setFocusPainted(false);
                setContentAreaFilled(false);
                setMargin(new Insets(0,0,0,0));
            }
        }

        public LoginPanel (){
            setOpaque(false);
            nameField.setFont(msYaHei);
            nameField.setForeground(Color.GRAY);
            nameField.setHorizontalAlignment(SwingConstants.RIGHT);
            pwField.setFont(msYaHei);
            pwField.setForeground(Color.GRAY);
            pwField.setHorizontalAlignment(SwingConstants.RIGHT);
            pwField.setEchoChar('\0');
            setLayout(new GridBagLayout());
            loginHeadPanel.setLayout(new GridBagLayout());
            loginHeadPanel.setBackground(Color.blue);
            loginHeadPanel.setOpaque(false);
            headline.setFont(new Font("Dialog",1,48));
            headline.setForeground(Color.white);
            loginHeadPanel.add(headline/*, new GBC(0,0,1,1).setFill(GBC.BOTH).setWeight(100, 100)*/);
            loginTextPanel.setLayout(new GridBagLayout());
            loginTextPanel.setOpaque(false);
            username.setFont(msYaHei);
            password.setFont(msYaHei);
            nameField.addFocusListener(new NumFieldFocusListener());
            messageOnFail.setFont(msYaHei);
            //numDoc.addDocumentListener(new NumFieldDocumentListener());hintz
            logInButton.addActionListener(new ButtonListener());
            pwField.addFocusListener(new PwFieldFocusListener());
            loginTextPanel.add(messageOnFail , new GBC(3,0,1,1).setFill(GBC.HORIZONTAL).setWeight(5, 1));
            loginTextPanel.add(username, new GBC(0,1,1,1).setWeight(1, 1));
            loginTextPanel.add(password, new GBC(0,2,1,1).setWeight(1, 1));
            loginTextPanel.add(nameField, new GBC(1,1,5,1).setFill(GBC.HORIZONTAL).setWeight(5, 1));
            loginTextPanel.add(pwField, new GBC(1,2,5,1).setFill(GBC.HORIZONTAL).setWeight(5, 1));
            loginTextPanel.add(new JLabel("") , new GBC(6,1,1,1).setFill(GBC.BOTH).setWeight(1, 1));
            loginTextPanel.add(new JLabel("") , new GBC(6,2,1,1).setFill(GBC.BOTH).setWeight(1, 1));
            loginTextPanel.add(new JLabel("") , new GBC(0,4,2,1).setFill(GBC.BOTH).setWeight(2, 1));
            loginTextPanel.add(new JLabel("") , new GBC(5,4,2,1).setFill(GBC.BOTH).setWeight(2, 1));
            loginTextPanel.add(logInButton, new GBC(3,4,1,1).setFill(GBC.HORIZONTAL).setWeight(1, 1));
            //
            this.add(loginHeadPanel, new GBC(0,0,1,2).setFill(GBC.BOTH).setWeight(100, 66.66667));
            this.add(loginTextPanel, new GBC(0,2,1,3).setFill(GBC.BOTH).setWeight(100, 100));
        }

        public class NumberValidator extends PlainDocument {

            private static final long serialVersionUID = 1L;

            private int limit;

            public NumberValidator(int limit) {
                super();
                this.limit = limit;
            }

            public void insertString(int offset, String str, AttributeSet attr)
                    throws javax.swing.text.BadLocationException {
                if (str == null) {
                    return;
                }
                if ((getLength() + str.length()) <= limit) {
                    char[] upper = str.toCharArray();
                    int length = 0;
                    for (int i = 0; i < upper.length; i++) {
                        if (upper[i] >= '0' && upper[i] <= '9') {
                            upper[length++] = upper[i];
                        }
                    }
                    super.insertString(offset, new String(upper, 0, length), attr);
                }
            }

        }

        public class NumFieldFocusListener implements FocusListener {

            @Override
            public void focusGained(FocusEvent arg0) {
                if(nameField.getText().equals("用户名")) {
                    nameField.setText("");
                    //numField.setDocument(new NumberValidator(11));
                }
                nameField.setForeground(Color.BLACK);
                nameField.setHorizontalAlignment(SwingConstants.LEFT);
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                if(nameField.getText().equals("")) {
                    nameField.setDocument(new PlainDocument());
                    nameField.setText("用户名");
                    nameField.setForeground(Color.GRAY);
                    nameField.setHorizontalAlignment(SwingConstants.RIGHT);
                }
            }
        }

        public class PwFieldFocusListener implements FocusListener{

            @Override
            public void focusGained(FocusEvent arg0) {
                if(String.valueOf(pwField.getPassword()).equals("密码")) {
                    pwField.setText("");
                }
                pwField.setForeground(Color.BLACK);
                pwField.setHorizontalAlignment(SwingConstants.LEFT);
                pwField.setEchoChar('*');
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                if(String.valueOf(pwField.getPassword()).equals("")) {
                    pwField.setText("密码");
                    pwField.setForeground(Color.GRAY);
                    pwField.setHorizontalAlignment(SwingConstants.RIGHT);
                    pwField.setEchoChar('\0');
                }
            }
        }

        public class ButtonListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String username = String.valueOf(nameField.getText());
                String password = String.valueOf(pwField.getPassword());
                if(true/*!logIn(id,password)*/) {
                    messageOnFail.setText("登录失败");
                    messageOnFail.setForeground(Color.RED);
                }
            }
        }
    }

    public void close() {
        this.dispose();
    }

    public static class GBC extends GridBagConstraints
    {

        public GBC(int gridx, int gridy)
        {
            this.gridx = gridx;
            this.gridy = gridy;
        }


        public GBC(int gridx, int gridy, int gridwidth, int gridheight)
        {
            this.gridx = gridx;
            this.gridy = gridy;
            this.gridwidth = gridwidth;
            this.gridheight = gridheight;
        }


        public GBC setAnchor(int anchor)
        {
            this.anchor = anchor;
            return this;
        }


        public GBC setFill(int fill)
        {
            this.fill = fill;
            return this;
        }


        public GBC setWeight(double weightx, double weighty)
        {
            this.weightx = weightx;
            this.weighty = weighty;
            return this;
        }


        public GBC setInsets(int distance)
        {
            this.insets = new Insets(distance, distance, distance, distance);
            return this;
        }


        public GBC setInsets(int top, int left, int bottom, int right)
        {
            this.insets = new Insets(top, left, bottom, right);
            return this;
        }


        public GBC setIpad(int ipadx, int ipady)
        {
            this.ipadx = ipadx;
            this.ipady = ipady;
            return this;
        }
    }
}



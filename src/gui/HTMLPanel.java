package gui;


import client.Main;
import model.JDBC;
import model.Messages;
import model.Users;

import javax.swing.*;
import java.util.Iterator;
import java.util.Vector;

public class HTMLPanel extends JEditorPane {
    Vector<Messages> messageList;
    private String htmlHead = "<!DOCTYPE html>\n" +
            "<html lang=\"zh-cn\">\n" +
            "    <head>\n" +
            "        <meta charset=\"UTF-8\">\n" +
            "<style type=\"text/css\">"+
            "body {font-family:Microsoft YaHei;}"+
            ".f_self {text-align:right;}"+
            ".f_other {text-align:left;}"+
            ".name {color:#3F48CC; font-size:12px;}"+
            ".time {color:#808080; font-size:10px;}"+
            ".content {font-size:18px; word-wrap:break-word; white-space:pre-wrap; white-space:-moz-pre-wrap;}"+
            ".f_system div {text-align:center; color:#808080; font-size:12px;}"+
            ".f_system span {text-align:center; color:#808080; font-size:10px;}"+
            "</style>"+
            "    </head>\n" +
            "    <body>";
    private String htmlTail = "</body></html>";
    //private Dialogue dialogue = new Dialogue();

    public HTMLPanel (Users target) {
        setContentType("text/html");
        messageList = JDBC.loadMessageList(Main.currentId,target.getId());
        //dialogue = JDBC.getDialogue(id);
        System.out.println(messageList.get(0));
        layoutComponent();
    }

    public HTMLPanel () {
        setContentType("text/html");
        messageList = JDBC.loadAnnounceList();
        //dialogue = JDBC.getDialogue(id);
        System.out.println(messageList.get(0));
        layoutComponent();
    }

    public void add (String str,int toId){
        messageList.add((new Messages(str,Main.currentId,toId)));
        layoutComponent();
        return;
    }

    public void remove (int i) {
        //dialogue.removeMessages(i);
        layoutComponent();
        return;
    }

    public void layoutComponent() {
        Iterator it = messageList.iterator();
        String text = ""+htmlHead;
        while(it.hasNext()) {
            text+=it.next();
        }
        text+=htmlTail;
        setText(text);
        repaint();
    }
}

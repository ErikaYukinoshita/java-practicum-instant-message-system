package gui;

import client.Main;
import model.Messages;
import model.Users;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Iterator;

public class MainWindow extends JFrame {
    public MainPanel mainPanel = new MainPanel();
    public TopLeftPanel topLeftPanel;
    public CentralPanel centralPanel;
    public TopRightPanel topRightPanel;
    public BottomPanel bottomPanel;

    public MainWindow() throws HeadlessException {
        setTitle("Demo");
        setLocation(800, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Exit the application and close the window
        setSize(1024, 768);
        JMenuBar mainMenuBar = new MainMenuBar();
        setJMenuBar(mainMenuBar);
        setContentPane(mainPanel);
        setVisible(true);
    }

    public class MainMenuBar extends JMenuBar {
        public MainMenuBar() {
            JMenu fileMenu = new JMenu("文件");
            JMenu messageMenu = new JMenu("消息");
            JMenu userMenu = new JMenu("用户");

            add(fileMenu);
            add(messageMenu);
            add(userMenu);

            JMenuItem uploadMenuItem = new JMenuItem("上传");
            JMenuItem downloadMenuItem = new JMenuItem("下载");

            fileMenu.add(uploadMenuItem);
            fileMenu.add(downloadMenuItem);

            JMenuItem issueDocumentMenuItem = new JMenuItem("传阅文档");
            JMenuItem issueMessageMenuItem = new JMenuItem("发送消息");

            messageMenu.add(issueDocumentMenuItem);
            messageMenu.add(issueMessageMenuItem);

            JMenuItem resignMenuItem = new JMenuItem("注销");

            userMenu.add(resignMenuItem);


        }
        /*class MessagePoster extends MessageWindow {
            public MessagePoster() throws HeadlessException {
                setTitle("发送公告");
                setLocation(800, 100);
                setSize(1024, 256);
                setContentPane(new MessagePanel());
                setVisible(true);
            }
            protected class MessagePanel extends JPanel {
                public MessagePanel() {
                    JTextArea txt2 = new JTextArea();
                    SimpleAttributeSet attrset = new SimpleAttributeSet();
                    StyleConstants.setFontSize(attrset, 24);
                    JScrollPane jsp2 = new JScrollPane(txt2);
                    jsp2.setBounds(0, 0, 100, 100);
                    jsp2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                    setLayout(new GridBagLayout());
                    //toolBar = new MainWindow.ToolBar();
                    //add(toolBar, new MainWindow.GBC(0, 5, 3, 1).setWeight(100, 0).setAnchor(GridBagConstraints.WEST));
                    add(jsp2, new MainWindow.GBC(0, 6, 3, 3).setWeight(100, 0).setFill(GridBagConstraints.BOTH).setIpad(10, 100));
                    ActionListener listener = new ActionListener() {
                        public void actionPerformed(ActionEvent event) {
                            txt1.add(txt2.getText(),U.getId());
                            txt2.setText("");
                        }
                    };
                    okButton.addActionListener(listener);
                    okButton.setSize(50, 50);
                    add(okButton, new MainWindow.GBC(0, 9, 3, 1).setAnchor(GridBagConstraints.EAST));
                    //eastPanel = new MainWindow.EastPanel();
                    //add(eastPanel, new MainWindow.GBC(3, 0, 1, 10).setFill(GridBagConstraints.BOTH).setWeight(0, 0).setIpad(150, 10));
                    txt1.layoutComponent();
                }
            }
        }*/

    }

    public class MainPanel extends JPanel {
        public MainPanel() {
            setLayout(new GridBagLayout());
            topLeftPanel = new TopLeftPanel();
            centralPanel = new CentralPanel();
            topRightPanel = new TopRightPanel();
            JScrollPane topRightScroll = new JScrollPane(topRightPanel);
            topRightScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            bottomPanel = new BottomPanel();
            this.add(topLeftPanel, new GBC(0, 0, 1, 3).setFill(GBC.BOTH).setIpad(20, 20).setWeight(100,300));
            this.add(centralPanel, new GBC(1, 0, 2, 3).setFill(GBC.BOTH).setIpad(20, 20).setWeight(200,300));
            this.add(topRightScroll, new GBC(3, 0, 1, 3).setFill(GBC.BOTH).setIpad(20, 20).setWeight(100,300));
            this.add(bottomPanel, new GBC(0, 3, 4, 2).setFill(GBC.BOTH).setIpad(20, 20).setWeight(400,200));
        }
    }

    public class TopLeftPanel extends JPanel {
        class MessageLabel extends JLabel {
            public MessageLabel(Messages M) {
            }
        }
        public TopLeftPanel() {
            setBorder(BorderFactory.createEtchedBorder());
            Iterator<Messages> it = Main.currentUser.messageList.iterator();
            for(;it.hasNext();) {
                this.add(new MessageLabel(it.next()));
            }
        }
    }

    public class CentralPanel extends HTMLPanel {
        public CentralPanel() {
            setBorder(BorderFactory.createEtchedBorder());
        }
    }

    public class TopRightPanel extends JPanel {
        public TopRightPanel() {
            setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
            setBorder(BorderFactory.createEtchedBorder());
            JLabel currentUser = new JLabel("当前用户名："+Main.currentUser.getUsername());
            currentUser.setFont(Main.MSYahei18);
            JLabel onlineUser = new JLabel("当前在线用户：");
            onlineUser.setFont(Main.MSYahei18);
            add(currentUser);
            add(onlineUser);
            Iterator it = Main.usersList.iterator();
            while(it.hasNext()) {
                Users u = (Users)it.next();
                if(u.getId()!=Main.currentId) {
                    UserLabel user = new UserLabel(u);
                    user.setFont(Main.MSYahei18);
                    add(user);
                }
            }
        }
        class UserLabel extends JLabel {
            Users user;
            MessageWindow mWindow;
            public UserLabel(Users U) {
                super(U.getUsername());
                user = U;
                this.addMouseListener(new UserLabelListener());
            }
            class UserLabelListener implements MouseListener {
                public void mouseClicked(MouseEvent e) {
                    // 处理鼠标点击
                    mWindow = new MessageWindow(user);
                }
                public void mouseEntered(MouseEvent e) {
                    // 处理鼠标移入
                }
                public void mouseExited(MouseEvent e) {
                    // 处理鼠标离开
                }
                public void mousePressed(MouseEvent e) {
                    // 处理鼠标按下
                }
                public void mouseReleased(MouseEvent e) {
                    // 处理鼠标释放
                }
            }
        }
    }

    public class BottomPanel extends JPanel {
        public BottomPanel() {
            setBorder(BorderFactory.createEtchedBorder());
            this.add(new JLabel("Bottom"));
        }
    }

    public static class GBC extends GridBagConstraints
    {
        public GBC(int gridx, int gridy)
        {
            this.gridx = gridx;
            this.gridy = gridy;
        }

        public GBC(int gridx, int gridy, int gridwidth, int gridheight)
        {
            this.gridx = gridx;
            this.gridy = gridy;
            this.gridwidth = gridwidth;
            this.gridheight = gridheight;
        }

        public GBC setAnchor(int anchor)
        {
            this.anchor = anchor;
            return this;
        }

        public GBC setFill(int fill)
        {
            this.fill = fill;
            return this;
        }

        public GBC setWeight(double weightx, double weighty)
        {
            this.weightx = weightx;
            this.weighty = weighty;
            return this;
        }

        public GBC setInsets(int distance)
        {
            this.insets = new Insets(distance, distance, distance, distance);
            return this;
        }

        public GBC setInsets(int top, int left, int bottom, int right)
        {
            this.insets = new Insets(top, left, bottom, right);
            return this;
        }

        public GBC setIpad(int ipadx, int ipady)
        {
            this.ipadx = ipadx;
            this.ipady = ipady;
            return this;
        }
    }
}


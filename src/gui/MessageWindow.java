package gui;

import client.Main;
import model.Users;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MessageWindow extends JFrame {
    JButton okButton = new JButton("发送");
    Users target;

    public MessageWindow() throws HeadlessException {
        setTitle("Demo");
        setLocation(800, 100);
        setSize(1024, 768);
        setContentPane(new MessagePanel(Main.currentUser));
        setVisible(true);
    }
    public MessageWindow(Users U) throws HeadlessException {
        target = U;
        setTitle(target.getUsername());
        setLocation(800, 100);
        setSize(1024, 768);
        setContentPane(new MessagePanel(target));
        setVisible(true);
    }

    protected class MessagePanel extends JPanel {
        public MessagePanel(Users U) {
            HTMLPanel txt1 = new HTMLPanel(U);
            JTextArea txt2 = new JTextArea();
            txt2.setFont(Main.MSYahei14);
            SimpleAttributeSet attrset = new SimpleAttributeSet();
            StyleConstants.setFontSize(attrset, 24);
            JScrollPane jsp1 = new JScrollPane(txt1);
            jsp1.setBounds(0, 0, 100, 100);
            jsp1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            JScrollPane jsp2 = new JScrollPane(txt2);
            jsp2.setBounds(0, 0, 100, 100);
            jsp2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            setLayout(new GridBagLayout());
            add(new JLabel(U.getUsername()), new MainWindow.GBC(0, 0, 3, 1).setWeight(100, 0).setAnchor(GridBagConstraints.WEST));
            add(jsp1, new MainWindow.GBC(0, 1, 3, 4).setWeight(100, 100).setFill(GridBagConstraints.BOTH));
            //toolBar = new MainWindow.ToolBar();
            //add(toolBar, new MainWindow.GBC(0, 5, 3, 1).setWeight(100, 0).setAnchor(GridBagConstraints.WEST));
            add(jsp2, new MainWindow.GBC(0, 6, 3, 3).setWeight(100, 0).setFill(GridBagConstraints.BOTH).setIpad(10, 100));
            ActionListener listener = new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    txt1.add(txt2.getText(),U.getId());
                    txt2.setText("");
                    Main.mainWindow.mainPanel.repaint();
                }
            };
            okButton.addActionListener(listener);
            okButton.setSize(50, 50);
            add(okButton, new MainWindow.GBC(0, 9, 3, 1).setAnchor(GridBagConstraints.EAST));
            //eastPanel = new MainWindow.EastPanel();
            //add(eastPanel, new MainWindow.GBC(3, 0, 1, 10).setFill(GridBagConstraints.BOTH).setWeight(0, 0).setIpad(150, 10));
            txt1.layoutComponent();
        }
    }
}

package model;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Vector;

public class JDBC {
    private static String driver = "com.mysql.jdbc.Driver";
    private static String url = "jdbc:mysql://localhost:3306/" + "javatest" + "?useUnicode=true&characterEncoding=utf8";
    private static String username = "root";
    private static String password = "114514";/*密码*/

    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static Vector<Users> getUserList() {
        String sql = "SELECT * FROM users";
        Vector<Users> list = new Vector<Users>();
        Users user;
        int id;
        String username;
        String password;
        boolean isAdmin;
        Connection conn = getConnection();
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                username = resultSet.getString("username");
                password = resultSet.getString("password");
                isAdmin = resultSet.getBoolean("isadmin");
                user = new Users(id,username,password,isAdmin);
                list.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static String getUsernameById (int id) {
        String sql = "select username from users where id = ?";
        String username = null;
        Connection conn = getConnection();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery(sql);
            while (resultSet.next()) {
                username = resultSet.getString("username");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return username;
    }

    public static Vector<Messages> loadMessageList(int fromId,int toId) {
        String sql = "SELECT * FROM messages WHERE (fromid = ? AND toid = ?) OR"+
                " (fromid = ? AND toid = ?)";
        Vector<Messages> list = new Vector<>();
        Messages message;
        boolean ispic;
        String content;
        LocalDateTime datetime;
        Connection conn = getConnection();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, fromId);
            preparedStatement.setInt(2, toId);
            preparedStatement.setInt(3, toId);
            preparedStatement.setInt(4, fromId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ispic = resultSet.getBoolean("ispic");
                content = resultSet.getString("content");
                datetime = resultSet.getTimestamp("datetime").toLocalDateTime();
                message = new Messages(ispic,content,datetime,resultSet.getInt("fromid"),resultSet.getInt("toid"));
                list.add(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static Vector<Messages> loadAnnounceList() {
        String sql = "SELECT * FROM messages WHERE toid = 0";
        Vector<Messages> list = new Vector<>();
        Messages message;
        boolean ispic;
        String content;
        LocalDateTime datetime;
        Connection conn = getConnection();
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ispic = resultSet.getBoolean("ispic");
                content = resultSet.getString("content");
                datetime = resultSet.getTimestamp("datetime").toLocalDateTime();
                message = new Messages(ispic,content,datetime,resultSet.getInt("fromid"),resultSet.getInt("toid"));
                list.add(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /*public static void insertMessage (Messages message) {
        String sql = "INSERT INTO messages WHERE toid = 0";
        Vector<Messages> list = new Vector<>();
        Messages message2;
        boolean ispic;
        String content;
        LocalDateTime datetime;
        Connection conn = getConnection();
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ispic = resultSet.getBoolean("ispic");
                content = resultSet.getString("content");
                datetime = resultSet.getTimestamp("datetime").toLocalDateTime();
                message = new Messages(ispic,content,datetime,resultSet.getInt("fromid"),resultSet.getInt("toid"));
                list.add(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }*/
}

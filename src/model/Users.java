package model;

import client.Main;

import java.util.Iterator;
import java.util.Vector;

public class Users {
    private int id;
    private String username;
    private String password;
    private boolean isAdmin = false;
    public Vector<Messages> messageList = new Vector<Messages>();
    public Users (String username, String password) {
        this.username = username;
        this.password = password;
        this.id = client.Main.usersList.size()+1;
    }
    public Users (int id, String username, String password, boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.id = id;
        this.isAdmin = isAdmin;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin() { this.isAdmin = true; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static String getUsernameById (int id) {
        Iterator it = Main.usersList.iterator();
        while(it.hasNext()) {
            Users user = (Users)it.next();
            if(user.getId()==id)
                return user.getUsername();
        }
        return "User not found";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

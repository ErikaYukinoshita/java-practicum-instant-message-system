package model;

import client.Main;

import java.time.LocalDateTime;

public class Messages {
    private boolean isPic = false;
    private String content;
    private LocalDateTime dateTime;
    private int fromId;
    private int toId;

    public Messages(boolean isPic, String content, LocalDateTime dateTime, int fromId, int toId) {
        this.isPic = isPic;
        this.content = content;
        this.dateTime = dateTime;
        this.fromId = fromId;
        this.toId = toId;
    }

    public Messages(boolean isPic, String picUrl, int fromId, int toId) {
        this.isPic = isPic;
        this.content = picUrl;
        this.dateTime = LocalDateTime.now().withNano(0);
        this.fromId = fromId;
        this.toId = toId;
    }

    public Messages(String content, int fromId, int toId) {
        this.content = content;
        this.dateTime = LocalDateTime.now().withNano(0);
        this.fromId = fromId;
        this.toId = toId;
    }

    public boolean isPic() {
        return isPic;
    }

    public void setPic(boolean pic) {
        isPic = pic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    @Override
    public String toString() {
        String fromName = /*"2";//*/Users.getUsernameById(fromId);
        String fromType = "f_other";
        String str;
        content = content.replaceAll("(\r\n|\r|\n|\n\r)", "<br />");
        if(fromId==Main.currentId)
            fromType = "f_self";
        if(fromId==0) {
            fromName = "系统消息";
            fromType = "f_system";
        }
        str = "<div class=\""+fromType+"\">"+
                "<div class=\"topline\">"+
                "<span class=\"name\">"+fromName+"</span> "+
                "<span class=\"time\">"+dateTime+"</span>"+" "+
                "</div>"+
                "<div class=\"content\">"+content+"</div>"+
                "</div>";
        return str;
    }
}

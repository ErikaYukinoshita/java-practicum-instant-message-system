package client;

import gui.LoginWindow;
import gui.MainWindow;
import gui.MessageWindow;
import model.JDBC;
import model.Messages;
import model.Users;

import java.awt.*;
import java.util.Vector;

public class Main {
    //预定义两种字体，微软雅黑14号和18号
    public static final Font MSYahei14 = new Font("Microsoft Yahei",Font.PLAIN,14);
    public static final Font MSYahei18 = new Font("Microsoft Yahei",Font.PLAIN,18);

    //定义一些全局的静态变量，例如当前用户，当前用户ID，以及窗体
    public static Users currentUser;
    public static int currentId;
    public static Vector<Users> usersList = JDBC.getUserList();
    public static LoginWindow loginWindow;
    public static MainWindow mainWindow;
    public static void main(String[] args) {
        //因为懒得登陆，所以从数据库里抓下标为1的用户出来
        currentUser = usersList.get(1);
        currentId = currentUser.getId();
        currentUser.messageList = new Vector<Messages>();
        loginWindow = new LoginWindow();
        mainWindow = new MainWindow();
    }
}

class UsersVector<E> extends Vector {
    public UsersVector() {

    }
}
